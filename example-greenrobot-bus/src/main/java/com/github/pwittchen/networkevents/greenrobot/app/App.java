package com.github.pwittchen.networkevents.greenrobot.app;

import android.app.Application;
import android.os.StrictMode;

public class App extends Application {

  @Override public void onCreate() {
    super.onCreate();
    setStrictMode();
  }

  private void setStrictMode() {
    StrictMode.setThreadPolicy(new StrictMode.ThreadPolicy.Builder().detectDiskReads()
    .detectDiskWrites()
    .detectNetwork()
    .penaltyLog()
    .build());

    StrictMode.setVmPolicy(new StrictMode.VmPolicy.Builder().detectLeakedSqlLiteObjects()
    .detectLeakedClosableObjects()
    .penaltyLog()
    .penaltyDeath()
    .build());
  }
}
