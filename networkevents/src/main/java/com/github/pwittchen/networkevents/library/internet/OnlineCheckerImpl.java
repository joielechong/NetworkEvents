/*
 * Copyright (C) 2015 Piotr Wittchen
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.github.pwittchen.networkevents.library.internet;

import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import com.github.pwittchen.networkevents.library.receiver.InternetConnectionChangeReceiver;
import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.Socket;

public final class OnlineCheckerImpl implements OnlineChecker {
  private static final String DEFAULT_PING_HOST = "www.google.com";
  private static final int DEFAULT_PING_PORT = 80;
  private static final int DEFAULT_PING_TIMEOUT_IN_MS = 2000;

  private final Context context;
  private static String pingHost;
  private static int pingPort;
  private static int pingTimeout;

  public OnlineCheckerImpl(Context context) {
    this.context = context;
    pingHost = DEFAULT_PING_HOST;
    pingPort = DEFAULT_PING_PORT;
    pingTimeout = DEFAULT_PING_TIMEOUT_IN_MS;
  }

  @Override public void check() {
    //new AsyncTask<Void, Void, Void>() {
    //  @Override protected Void doInBackground(Void... params) {
    //    boolean isOnline = false;
    //    try {
    //      Socket socket = new Socket();
    //      socket.connect(new InetSocketAddress(pingHost, pingPort), pingTimeout);
    //      isOnline = socket.isConnected();
    //      socket.close();
    //    } catch (IOException e) {
    //      isOnline = false;
    //    } finally {
    //      sendBroadcast(isOnline);
    //    }
    //    return null;
    //  }
    //}.execute();

    CheckTask task = new CheckTask(context);
    task.execute();
  }

  protected final class CheckTask extends AsyncTask<Void, Void, Void> {
    private Context context;
    CheckTask(Context context) {
      this.context = context;
    }
    @Override protected Void doInBackground(Void... voids) {
      boolean isOnline = false;
        try {
          Socket socket = new Socket();
          socket.connect(new InetSocketAddress(pingHost, pingPort), pingTimeout);
          isOnline = socket.isConnected();
          socket.close();
        } catch (IOException e) {
          isOnline = false;
        } finally {
          sendBroadcast(context, isOnline);
        }
        return null;
    }
  }
  @Override public void setPingParameters(String host, int port, int timeoutInMs) {
    pingHost = host;
    pingPort = port;
    pingTimeout = timeoutInMs;
  }

  private static void sendBroadcast(Context context, boolean isOnline) {
    Intent intent = new Intent(InternetConnectionChangeReceiver.INTENT);
    intent.putExtra(InternetConnectionChangeReceiver.INTENT_EXTRA, isOnline);
    context.sendBroadcast(intent);
  }
}
